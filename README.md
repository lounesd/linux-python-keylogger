# Python Keylogger ! Authorized or educational purposes only !

## Introduction :
Keyloggers for Linux in Python for authorized or educational purposes only. Tested on debian 12.

## Requirements :

 `pip install pynput`

Note : Without virtual environment you need to add `--break-system-packages`

## Execution :

 `python keylogger.py`

To stop the keylogger just stop the execution of the script (ex. CTRL + C)

## Description :
keylogger-to-file.py : output in the file "keystrokes.txt"

keylogger-to-udp.py : output over udp on port 1234 to any address

! Authorized or educational purposes only. !